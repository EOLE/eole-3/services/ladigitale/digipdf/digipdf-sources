# DigiPDF

DigiPDF regroupe une série d’outils pour travailler sur les fichiers PDF. L’application repose sur le service libre Stirling PDF (https://github.com/Stirling-Tools/Stirling-PDF).

Elle est publiée sous licence GNU GPLv3.

### Compilation du fichier jar (Java 17 requis)
```
gradle build
```

### Lancement du serveur Tomcat
```
cd build/libs
java -jar DigiPDF-0.29.0.jar
```

### Démo
https://digipdf.app

### Soutien
Open Collective : https://opencollective.com/ladigitale

Liberapay : https://liberapay.com/ladigitale/

